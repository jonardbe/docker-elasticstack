# elkvue

## Setting up docker

### .env

Create the .env file like so...

```cmd
# Password for the 'elastic' user (at least 6 characters)
ELASTIC_PASSWORD=

# Password for the 'kibana_system' user (at least 6 characters)
KIBANA_PASSWORD=

# Password for the logstash_system user
LOGSTASH_PASSWORD=

# Password for the logstash_writer user
LOGSTASH_WRITER_PASSWORD=

# Version of Elastic products
STACK_VERSION=8.0.0

# Set the cluster name
CLUSTER_NAME=docker-cluster

# Set to 'basic' or 'trial' to automatically start the 30-day trial
LICENSE=basic
#LICENSE=trial

# Port to expose Elasticsearch HTTP API to the host
ES_PORT=9200
#ES_PORT=127.0.0.1:9200

# Port to expose Kibana to the host
KIBANA_PORT=5601
#KIBANA_PORT=80

# Increase or decrease based on the available host memory (in bytes)
MEM_LIMIT=1073741824

```

### Memory

The vm.max_map_count setting must be set in the docker-desktop container:

```console
wsl -d docker-desktop
sysctl -w vm.max_map_count=262144
exit
```

### Start and stop the elk stack

```console
docker-compose up -d --build
docker-compose down -v
```
